import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module'; 
import { ValidationPipe } from '@nestjs/common';
import { Logger } from '@nestjs/common'; 
import * as config from 'config'


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const logger = new Logger('bootstrap');

  
  const serverConfig = config.get('server'); 
  // console.log(serverConfig) // it will console litstening on port 3000, take the value from default.yaml
  



  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true
    })
  )
  const port = process.env.PORT || serverConfig.port // if PORT is not defined , it fall back to serverConfig.port
  await app.listen(port);




  logger.log(`application is litstening on port ${port}`)

}
bootstrap();
